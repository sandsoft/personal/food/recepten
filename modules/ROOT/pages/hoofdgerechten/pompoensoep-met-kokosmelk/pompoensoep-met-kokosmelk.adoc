= Pompoensoep met kokosmelk

* *Voor*: 4 personen;
* *Bereiding*: 45 minuten;

== Ingrediënten

* 4 eetlepels olijfolie;
* 1 pompoen;
* 1 ui;
* 3 tenen knoflook;
* 3 cm gember;
* 2 tl kurkuma (of currie poeder);
* 1/4 tl kaneel;
* 1 snuf chilivlokken;
* 400 ml kokosmelk;
* 1.2 liter groentebouillon;
* peper en zout;
* 1 kneepje citroensap;

=== Toppings

* 150 ml kokosmelk;
* 15 g salie;

== Bereidingswijze

. Schil de pompoen en snijd in blokjes van 2 cm.
Snijd de ui en knoflook grof.
Rasp de gember fijn.

. Verhit de olie in een grote pan met dikke bodem.
Bak de ui en pompoen op middelhoog vuur voor 8 minuten tot ze beginnen te karamelliseren.
Voeg de knoflook en geraspte gember toe en bak 2 minuten mee.
Voeg dan de kurkuma, kaneel en chilivlokken toe en bak nog 2 minuten.

. Voeg de kokosmelk en groentebouillon toe en breng de soep aan de kook.
Zet daarna het vuur lager en kook voor 20 minuten tot de pompoen gaar is.
Pureer de soep glad en breng op smaak met een kneepje citroensap.

. Bak de salie in 2 el olijfolie krokant.
Laat uitlekken op keukenpapier.

. Schuim de kokosmelk in een melkschuimer of met een staafmixer.

. Serveer de soepen in diepe kommen.
Verdeel het kokosschuim over de kommen en maak af met de krokante salieblaadjes.

== Bron

https://www.24kitchen.nl/recepten/pompoensoep-met-kokosmelk
