= Vegetarische tajine met pompoen, zoete aardappel en feta

== Ingrediënten

Voor 3 personen

* Half blok feta;
* 2 zoete aardappelen (die met oranje vruchtvlees zijn het zoetst);
* 1 kleine winterwortel;
* 1 butternut squash (ik vond hele kleintjes bij de turk anders gewoon de helft van een grote);
* 2 grote tomaten;
* wat gedroogde abrikozen of ander tuttifrutti fruit;
* half (tuinkruiden-)bouillonblokje;
* 1 eetlepel Ras en Hanout (Marokkaans specerijnmengsel);
* 1 theelepel kaneel;
* 1 theelepel komijnpoeder;
* 2 flinke tenen knoflook;
* rijst (of couscous) voor 4 personen;
* eventueel een handje geroosterde sesamzaadjes;

== Bereidingswijze

Aardappelen en pompoen in hapklare blokken snijden, wortel in halve schijven van ongeveer een halve cm dik.
De knoflooktenen in vieren, als ze gestoofd zijn zijn die stukken lekker zoet.
In een tajine (of gewoon een braadpan) olie verhitten en de wortel en knoflook even aanbakken.
Vuur zacht en met de deksel erop 5 minuutjes stoven.
Dan de pompoen, aardappel, tomaten (in grove stukken), specerijen erbij, bouillonblokje erover kruimelen en 1 of 2 deciliter heet water erover (tot halverwege de berg groenten ongeveer) gieten.
Deksel er weer op en 10 minuten laten stoven, af en toen even roeren en kijken of het niet te hard gaat.
Dan het gedroogde fuit er bij, nog 5 of tien minuten tot alles lekker gaar is.

Feta er over verkruimelen en eventueel nog wat geroosterde sesamzaadjes

== Bron

https://www.smulweb.nl/recepten/1147495/Vegetarische-tajine-met-pompoen-zoete-aardappel-en-feta
