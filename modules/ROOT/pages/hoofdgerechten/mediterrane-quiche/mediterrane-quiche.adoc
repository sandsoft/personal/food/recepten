= Mediterrane Quiche

== Ingrediënten

* 1 xref:basisrecepten/basisrecept-hartige-taartbodem/basisrecept-hartige-taartbodem.adoc[basisrecept voor hartige taartbodem] of 1 pakje roomboter bladerdeeg
* 1 courgette
* 1 aubergine
* scheutje olijfolie
* 1 teentje knoflook
* peper en zout
* 1 paprika
* 5 eieren
* 200 ml slagroom
* 5 blaadjes verse basilicum
* 75 g geraspte Parmezaanse kaas
* ongeveer 10 olijven, groen of zwart, of beide
* 35 g pijnboompitjes
* 8 cherrytomaatjes
* provençaalse kruiden

== Bereiding

. Bereid de bodem.
. Verwarm de oven voor op 180 °C.
. Snijd de courgette en aubergine in plakjes en gril ze op een grillpan.
. Doe de gegrilde groenten in een kom en giet er een klein beetje olijfolie over, pers er een teentje knoflook op uit en voeg wat peper en zout toe.
Hussel door elkaar.
. Blaker de paprika rondom zwart in de vlam van het gasfornuis en laat hem afkoelen in een gesloten plastic zakje.
. Veeg het velletje ervan af en snijd de paprika in reepjes en bestrooi met wat peper en zout.
. Klop de eieren door de room en voeg wat peper en zout toe.
. Roer de helft van de geraspte kaas er doorheen.
. Verdeel alle groenten met de olijven en basilicum over de bodem en giet de roomvulling er voorzichtig overheen.
. Strooi de kaas en de pijnboompitjes over de quiche en verdeel de tomaatjes erover.
. Bestrooi de quiche met nog wat gedroogde provençaalse kruiden en bak hem in de oven gedurende 25-30 minuten.
. Haal de quiche uit de oven en laat hem minstens 10 minuten afkoelen.

== Bron

De Veldkeuken - het kookboek (blz. 168)
