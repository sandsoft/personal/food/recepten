= Plaattaart met kip en pompoen

== Ingrediënten

Voor 4 personen

- ½ flespompoen
- 3 rode uien
- 2 el milde olijfolie
- 395g kipfilethaasjes sweet chili
- 270g vers bladerdeeg
- 50g hazelnoten
- 1 middelgroot ei
- 2 el zuivelspread
- 2 el keukenstroop

== Bereiding

. Verwarm de oven voor op 200 °C.
Verwijder uit de pompoen de zaden en de draderige binnenkant.
Snijd de pompoen in blokjes van 1 cm.
Breng een pan met water aan de kook en kook de blokjes 4 min. zacht.
Giet af en pureer met de staafmixer.
Laat afkoelen.

. Snipper ondertussen de rode uien.
Verhit de olie in een koekenpan en stoof de ui op laag vuur 5 min. tot zacht.


. Snijd de kiphaasjes in vieren.
Rol het deeg met bakpapier uit over een bakplaat en prik het deeg in met een vork.
Hak de noten grof.

. Klop het ei los.
Meng de pompoen, ¾ van het ei en de zuivelspread, breng op smaak met peper en zout.
Bestrijk met het pompoenmengsel, verdeel de ui, kipfilet en hazelnoten erover.
Bestrijk de deegrand met de rest van het ei.

. Bak de taart ca. 20 min. in de oven.
Besprenkel met de keukenstroop.

== Bron

- https://www.ah.nl/allerhande/recept/R-R1195713/plaattaart-met-kip-en-pompoen
