= Mexicaanse Bonensoep

* *Voor*: 2 personen;
* *Voorbereiding*: 20 minuten;
* *Bereiding*: 40 minuten;

== Ingrediënten

* 2 el olijf- of kokosolie;
* 2 uien, gesnipperd;
* 3 teentjes knoflook, geperst;
* 350 g tomaten, in stukjes;
* 5 dl groentebouillon;
* 400 g kidneybonen uit blik, afgespoeld en uitgelekt;
* 1 tl gerookte paprikapoeder;
* 1 tl zeezout;
* 1 tl gedroogde oregano;
* 1 el gedroogde anchopepers, of 1 tl ancho- of gewone chilipoeder;
* 200 g zwarte bonen uit blik, afgespoeld en uitgelekt;

Voor erbij:

* veganistische zure room of zuivelvrij yoghurt;
* stukjes avocado;
* partjes limoen;
* verkruimelde tortillachips;
* dunne ringen rode chilipeper;
* verse koriander, gehakt;

== Bereidingswijze

Verhit de olie een grote pan met dikke bodem op halfhoog vuur.
Voeg de uien en knoflook toe en bak ze in 10 minuten glazig.
Voeg de tomaten, bouillon, kidneybonen, het paprikapoeder, zout, de oregano en anchopepers toe.
Breng het mengsel aan de kook en laat het 5 minuten sudderen.

Maak de massa met een staafmixer glad.
Voeg dan de zwarte bonen toe en kook deze nog 15 minuten mee, of tot de vloeistof is ingekookt en de zwarte bonen goed warm zijn.
Schep een beetje zure room op de soep en strooi er stukjes avocado over.
Knijp er ook wat limoensap over uit, bestrooi de soep met verkruimelde tortillachips, ringetjes rode chilipeper en gehakte koriander en dien op.
